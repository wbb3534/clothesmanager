package com.seop.clothesmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClothesManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ClothesManagerApplication.class, args);
    }

}
