package com.seop.clothesmanager.controller;

import com.seop.clothesmanager.model.ClothesRequest;
import com.seop.clothesmanager.service.ManagerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/manager")
@RequiredArgsConstructor
public class ManagerController {
    private final ManagerService managerService;

    @PostMapping("/data")
    public String getData(@RequestBody ClothesRequest request) {
        managerService.setData(request.getClothesType(), request.getClothesColor(), request.getClothesSize(), request.getOwner(), request.getBrand());
        return "OK";
    }
}
