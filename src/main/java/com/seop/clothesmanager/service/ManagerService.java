package com.seop.clothesmanager.service;

import com.seop.clothesmanager.entity.Clothes;
import com.seop.clothesmanager.repository.ManagerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ManagerService {
    private final ManagerRepository managerRepository;

    public void setData(String clothesType, String clothesColor, String clothesSize, String owner, String brand) {
        Clothes addData = new Clothes();
        addData.setClothesType(clothesType);
        addData.setClothesColor(clothesColor);
        addData.setClothesSize(clothesSize);
        addData.setOwner(owner);
        addData.setBrand(brand);

        managerRepository.save(addData);
    }
}
