package com.seop.clothesmanager.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClothesRequest {
    private Long id;
    private String clothesType;
    private String clothesColor;
    private String clothesSize;
    private String owner;
    private String brand;

}
