package com.seop.clothesmanager.repository;

import com.seop.clothesmanager.entity.Clothes;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ManagerRepository extends JpaRepository<Clothes, Long> {
}
