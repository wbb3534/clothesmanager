package com.seop.clothesmanager.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Setter
@Getter
public class Clothes {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 10)
    private String clothesType;

    @Column(nullable = false, length = 10)
    private String clothesColor;

    @Column(nullable = false, length = 10)
    private String clothesSize;

    @Column(nullable = false, length = 10)
    private String owner;

    @Column(nullable = false, length = 10)
    private String brand;

}
